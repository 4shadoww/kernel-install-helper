#!/bin/env python3

import sys
import os
import subprocess

def want_to_continue():
    answer = input("do you wish to continue? [y/N]")

    if answer == "" or answer.isspace() or answer.lower() == "n": return False

    elif answer.lower() == "y": return True

    return want_to_continue()



def print_usage():
    print("usage:", sys.argv[0], "[build] [old_kernel] [new_kernel]\n")
    print("example:", sys.argv[0], "5.11.14", "5.11.15\n")
    print("parameters:")
    print("build:\t\tkernel build directory root")
    print("old kernel:\tkernel version to be cleaned")
    print("new kernel:\tkernel version to be installed")


def main():
    root = False
    superuserdo = "sudo"
    compress = "lz4"

    if len(sys.argv) < 3:
        print_usage()
        sys.exit(1)

    if subprocess.run(["ls", "/root"], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL) == 0: root = True

    if root: superuserdo = ""
    else: superuserdo += " "

    oldmajorminor = ''.join(sys.argv[2].split('.')[0:2])
    newmajorminor = ''.join(sys.argv[3].split('.')[0:2])
    build = sys.argv[1]
    if not build.endswith('/'): build += '/'

    clean_up_commands = [superuserdo+"rm /boot/vmlinuz-linux"+oldmajorminor,
                         superuserdo+"rm /boot/System.map",
                         superuserdo+"rm /boot/System.map-custom-linux",
                         superuserdo+"rm /boot/initramfs-linux"+oldmajorminor+".img"]

    print("cleaning old kernel...")

    for command in clean_up_commands:
        status = os.system(command)
        if status != 0:
            print("cleaning old kernel failed!")
            if not want_to_continue(): sys.exit(1)

    print("cleaning done")

    print("installing new kernel")

    install_commands = [superuserdo+"cp -v "+build+"arch/x86_64/boot/bzImage /boot/vmlinuz-linux"+newmajorminor,
                        superuserdo+"mkinitcpio -k "+sys.argv[3]+ " -z "+compress+" -g /boot/initramfs-linux"+newmajorminor+".img",
                        superuserdo+"cp "+build+"System.map /boot/System.map-custom-linux",
                        superuserdo+"cp "+build+"System.map /boot/System.map"]

    for command in install_commands:
        status = os.system(command)
        if status != 0:
            print("installing new kernel failed!")
            if not want_to_continue(): sys.exit(1)

    print("updating grub...")

    os.system(superuserdo+"grub-mkconfig -o /boot/grub/grub.cfg")

if __name__ == '__main__':
    main()
